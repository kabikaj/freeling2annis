#!/usr/bin/env python3
#
#    createPepperWorkflow.py
#
# create workflow job for pepper
#
# dependencies:
#   * python module bs4
#
# AUTHOR:
#   Alicia Gonzalez Martinez - https://gitlab.com/kabikaj
#
# example:
#   $ python createPepperWorkflow.py --inpath data/homage_to_catalonia_xml \
#                                    --outpath data/homage_to_catalonia_annis \
#                                    --name homage_to_catalonia_corpus \
#                                    --meta data/homage_to_catalonia.meta > homage_to_catalonia_workflow.pepper
#
###############################################################################################################

import os
import sys
import shutil
import argparse
from bs4 import BeautifulSoup


CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='create pepper workflow file')
    parser.add_argument('--inpath', required=True, help='xml directory')
    parser.add_argument('--outpath', required=True, help='annis directory')
    parser.add_argument('--name', required=True, help='name of the corpus')
    parser.add_argument('--meta', nargs='?', help='metadata file')
    args = parser.parse_args()

    inpath_base = os.path.basename(args.inpath)

    soup = BeautifulSoup(features='xml')
    pepper_job = soup.new_tag('pepper-job', id='%s-job' % inpath_base, version='1.0')

    importer = soup.new_tag('importer', path='./%s' % args.inpath)
    importer['name'] = 'GenericXMLImporter'

    custom_importer = soup.new_tag('customization')

    if args.meta:
        shutil.copyfile(args.meta, os.path.join(args.inpath, '%s.meta' % inpath_base))
        prop_readmeta = soup.new_tag('property', key='pepper.before.readMeta')
        prop_readmeta.string = 'meta'
        custom_importer.append(prop_readmeta)

    prop_ending = soup.new_tag('property', key='genericXml.importer.file.endings')
    prop_ending.string = 'xml'
    custom_importer.append(prop_ending)

    prop_sDocument = soup.new_tag('property', key='genericXml.importer.sMetaAnnotation.sDocument')
    prop_sDocument.string = '//b, //b//'
    custom_importer.append(prop_sDocument)

    importer.append(custom_importer)
    pepper_job.append(importer)

    exporter = soup.new_tag('exporter', path='./%s' % args.outpath)
    exporter['name'] = 'ANNISExporter'

    custom_exporter = soup.new_tag('customization')

    prop_name = soup.new_tag('property', key='corpusName')
    prop_name.string = args.name
    custom_exporter.append(prop_name)

    exporter.append(custom_exporter)
    pepper_job.append(exporter)

    soup.append(pepper_job)

    print(soup.prettify(), file=sys.stdout)
