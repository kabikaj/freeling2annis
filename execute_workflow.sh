#!/bin/bash
#
#    execute_workflow.sh
#
# perform POS tagging with freeling to input text files, convert to xml
# and then convert to annis using pepper
#
# dependencies:
#   * python 3.5+
#   * python module bs4
#   * java 1.7+
#   * javac 1.7+
#   * freeling 4.0
#   * freeling2xml.py
#   * createPepperWorkflow.py
#   * pepper (GenericXMLImporter, ANNISExporter)
#
# Languages:
#   as  asturian
#   ca  catalan
#   de  german
#   fr  french
#   gl  galician
#   it  italian
#   pt  portuguese
#   ru  russian
#   sl  slovene
#   es  spanish
#   en  english
#   cy  welsh
#
# AUTHOR:
#   Alicia Gonzalez Martinez - https://gitlab.com/kabikaj
#
# examples:
#   $ bash execute_workflow.sh -i data/homage_to_catalonia -x data/homage_to_catalonia_xml \
#                              -a data/homage_to_catalonia_annis -l en -c homage_to_catalonia_corpus \
#                              -m data/homage_to_catalonia.meta -v
#
#   $ bash execute_workflow.sh -i data/chronicas_de_viagem -x data/chronicas_de_viagem_xml \
#                              -a data/chronicas_de_viagem_annis -l pt -c chronicas_de_viagem_corpus \
#                              -m data/chronicas_de_viagem.meta -v
#
#   $ bash execute_workflow.sh -i data/aus_den_tiefen_des_weltmeeres -x data/aus_den_tiefen_des_weltmeeres_xml \
#                              -a data/aus_den_tiefen_des_weltmeeres_annis -l de -c aus_den_tiefen_des_weltmeeres_corpus \
#                              -m data/aus_den_tiefen_des_weltmeeres.meta -v
#
#######################################################################################################

#
# variables
#

PROJECT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PEPPER_PATH="$PROJECT_PATH"/pepper/

RED_COLOR=$'\e[1;31m'
END_COLOR=$'\e[0m'

OPTIND=1

HELP=\
"usage: bash $0 [-h] -i INPUT_PATH -x XML_PATH -a ANNIS_PATH -l (as|ca|de|fr|gl|it|pt|ru|sl|es|en|cy) -c CORPUS_NAME [-m META_FILE] [-v]\n"\
'\n'\
'parse text with freeling and convert to xmi using pepper\n'\
'\n'\
'optional arguments:\n'\
'  -h              show this help message and exit\n'\
'  -i INPUT_PATH   path containing input texts\n'\
'  -x XML_PATH     path for xml conversion (*must* exist)\n'\
'  -a ANNIS_PATH   path for creating annis db\n'\
'  -l LANGUAGE     language (as|ca|de|fr|gl|it|pt|ru|sl|es|en|cy)\n'\
'  -c CORPUS_NAME  name of the corpus\n'\
'  -m META_FILE    config file with corpus metadata\n'\
'  -v              verbose mode'

INPUT_PATH=""
XML_PATH=""
ANNIS_PATH=""
LANGUAGE=""
CORPUS_NAME=""
META_CORPUS=""
VERBOSE=0

########################################

#
# functions
#

parse_args()
{
  while getopts ':hei:x:a:l:c:m:v' opt
  do
    case "$opt" in
      'h') echo -e "$HELP" ; exit 0 ;;
      'i') INPUT_PATH=$OPTARG ;;
      'x') XML_PATH=$OPTARG ;;
      'a') ANNIS_PATH=$OPTARG ;;
      'l') LANGUAGE=$OPTARG ;;
      'c') CORPUS_NAME=$OPTARG ;;
      'm') META_CORPUS=$OPTARG ;;
      'v') VERBOSE=1 ;;
      '?') echo -e "${RED_COLOR}option -$OPTARG not valid${END_COLOR}\n\n$HELP" >&2 ; exit 1 ;;
    esac
  done

  shift $((OPTIND-1))
  [ "$1" = "--" ] && shift

  if [ -z "$INPUT_PATH" ]; then
    echo -e "${RED_COLOR}the following arguments are required: -i${END_COLOR}\n\n$HELP" >&2 ; exit 1
  fi

  if [ ! -d "$INPUT_PATH" ]; then
    echo -e "${RED_COLOR}path $INPUT_PATH not found${END_COLOR}\n\n$HELP" >&2 ; exit 1
  fi

  if [ -z "$XML_PATH" ]; then
    echo -e "${RED_COLOR}the following arguments are required: -x${END_COLOR}\n\n$HELP" >&2 ; exit 1
  fi

  if [ ! -d "$XML_PATH" ]; then
    echo -e "${RED_COLOR}path $XML_PATH not found${END_COLOR}\n\n$HELP" >&2 ; exit 1
  fi

  if [ -z "$ANNIS_PATH" ]; then
    echo -e "${RED_COLOR}the following arguments are required: -a${END_COLOR}\n\n$HELP" >&2 ; exit 1
  fi

  if [ -z "$LANGUAGE" ]; then
    echo -e "${RED_COLOR}the following arguments are required: -l${END_COLOR}\n\n$HELP" >&2 ; exit 1
  fi

  case "$LANGUAGE" in
    as|ca|de|fr|gl|it|pt|ru|sl|es|en|cy)
      ;;
    *)
      echo -e "${RED_COLOR}value of argument -l must be (es|it|en)${END_COLOR}\n\n$HELP" >&2 ; exit 1
      ;;
  esac

  if [ -z "$CORPUS_NAME" ]; then
    echo -e "${RED_COLOR}the following arguments are required: -c${END_COLOR}\n\n$HELP" >&2 ; exit 1
  fi

}

########################################

#
# main
#

parse_args $@

for DOC in $(find $INPUT_PATH -type f -name "*.txt") ; do

  DOCNAME=${DOC##*/}
  BASE=${DOCNAME%.txt}
  METAFILE=$INPUT_PATH/$BASE.ini

  ARG_META="" # metadata for document

  if [ -f $METAFILE ]; then
    ARG_META="--meta $METAFILE"
  fi

  if [ $VERBOSE -eq 1 ]; then
      echo -e ":: tokenising, POS tag and converting to xml file $DOC..." 1>&2
  fi

  cat $DOC | analyze -f $LANGUAGE.cfg | python3 "$PROJECT_PATH"/freeling2xml.py $ARG_META > "$XML_PATH"/"$BASE".xml

done


ARG_META="" # metadata for corpus

if [ ! -z $META_CORPUS ]; then
  ARG_META="--meta $META_CORPUS"
fi

PEPPER_WORKFLOW="$PROJECT_PATH"/"$(basename $INPUT_PATH)"_workflow.pepper

if [ $VERBOSE -eq 1 ]; then
      echo -e ":: creating pepper workflow $PEPPER_WORKFLOW..." 1>&2
fi

python3 createPepperWorkflow.py --inpath $XML_PATH --outpath $ANNIS_PATH --name $CORPUS_NAME $ARG_META > $PEPPER_WORKFLOW

if [ $VERBOSE -eq 1 ]; then
      echo -e ":: executing pepper workflow in $PEPPER_PATH and dump output to $ANNIS_PATH/..." 1>&2
fi

cd $PEPPER_PATH
bash pepperStart.sh $PEPPER_WORKFLOW

