#!/usr/bin/env python3
#
#    freeling2xml.py
#
# convert freeling output format to simple xml to be parsed by pepper GenericXMLImporter
#
# dependencies:
#   * python module bs4
#
# AUTHOR:
#   Alicia Gonzalez Martinez - https://gitlab.com/kabikaj
#
# example:
#   $ cat homage_to_catalonia/homage_to_catalonia.chapter_1.txt | analyze -f en.cfg | python freeling2xml.py \
#     --meta homage_to_catalonia/homage_to_catalonia.chapter_1.ini
#
##############################################################################################################

import os
import sys
import argparse
from bs4 import BeautifulSoup
from configparser import ConfigParser, MissingSectionHeaderError


def loadconfig(cfgpath, globals=globals()):
    """ Load metadata file.

    Args:
        cfgpath (String): path of configuration file.
        globals (dict): global variables, to keep track of the module path.

    Returns:
        list: sequence of attribute, value tuples of metadata or
            empty list if cfgpath is None

    Raise:
        ValueError: if any error is found in config file.

    """
    if not cfgpath:
        return list()

    cfg = ConfigParser(inline_comment_prefixes=('#'))
    cfg.optionxform = str # make parser case sensitive

    try:
        cfg.read(cfgpath)
            
    except MissingSectionHeaderError:
        raise ValueError('Error in module {__file__}: '\
                         'no sections in config file {cfgpath}'.format(**globals, **locals()))
    
    if not cfg.sections():
        raise ValueError('Error in module {__file__}: '\
                         'config file {cfgpath} missing or empty'.format(**globals, **locals()))
    
    return cfg.items('metadata')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='convert freeling output to xml')
    parser.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin, help='tagged text file')
    parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout, help='output file')
    parser.add_argument('--meta', nargs='?', help='metadata file path')
    args = parser.parse_args()

    try:
        meta = loadconfig(args.meta)
    except ValueError as err:
        print(err, file=sys.stderr)
        sys.exit(1)
    
    soup = BeautifulSoup(features='xml')
    soup.append(soup.new_tag('a'))

    meta_tag = soup.new_tag('b')

    for atr, val in meta:
        meta_tag[atr] = val

    soup.a.append(meta_tag)

    soup.a.append(soup.new_tag('content'))

    for form, lemma, tag, _ in (l.split(None,3) for l in filter(str.strip, args.infile)):
        tok = soup.new_tag('t', lemma=lemma, POS=tag)
        tok.string = form.replace('_', ' ') # restore spaces in multiwords
        soup.a.content.append(tok)

    print(soup.prettify(), file=args.outfile)
