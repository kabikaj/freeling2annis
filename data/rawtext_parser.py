#!/usr/bin/env python3
#
#    rawtext_parser.py
#
# split text into chapters and save chapters in separate files and create metadata
# files for each chapter
# 
# store all files within directory with same name as rawtext main file
#
# AUTHOR:
#   Alicia Gonzalez Martinez - https://gitlab.com/kabikaj
#
# examples:
#   $ python rawtext_parser.py homage_to_catalonia.txt --skip_begin
#   $ python rawtext_parser.py chronicas_de_viagem.txt --regex "[IXV]+ +[^ ]+" --skip_begin
#   $ python rawtext_parser.py aus_den_tiefen_des_weltmeeres.txt --regex "^[0-9][^0-9.]+" --skip_begin
#
######################################################################################################

import os
import re
import sys
import argparse
import configparser
from itertools import groupby

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))
DEFAULT_REGEX = '^Chapter +\d{1,3} *$'


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='split text of book into chapters')
    parser.add_argument('infile', type=argparse.FileType('r'), help='raw text')
    parser.add_argument('--regex', default=DEFAULT_REGEX, help='regex to match chapter names [default "%s"]' % DEFAULT_REGEX)
    parser.add_argument('--skip_begin', action='store_true', help='skip text before first chapter')
    args = parser.parse_args()

    chapter_name = re.compile(r'%s' % args.regex)
    blocks = groupby(args.infile, chapter_name.match)
    chapters = (''.join('%s ' % l.strip() if l.strip() else l for l in lines) for _, lines in blocks)
    
    if args.skip_begin:
        next(chapters)

    BASENAME = os.path.splitext(args.infile.name)[0]

    OUTPATH = os.path.join(CURRENT_PATH, BASENAME)
    if not os.path.exists(OUTPATH):
        os.makedirs(OUTPATH)
    
    for title, body in zip(*[iter(chapters)]*2):
    
        norm_title = title.strip().replace(' ','_').lower()
        outfname = os.path.join(CURRENT_PATH, os.path.join(OUTPATH, '%s.%s.txt' % (BASENAME, norm_title)))
    
        with open(outfname, 'w') as outfp:
            
            print(title, file=outfp)
            print(body, file=outfp)

        metafname = os.path.join(CURRENT_PATH, os.path.join(OUTPATH, '%s.%s.ini' % (BASENAME, norm_title)))

        cfg = configparser.ConfigParser()

        with open(metafname, 'w') as metafp:

            cfg['metadata'] = {}
            cfg['metadata']['chapter_title'] = title
            cfg.write(metafp)
