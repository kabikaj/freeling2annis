VII Figueira da Foz 


Desde S. Martinho do Porto até á Marinha Grande a linha ferrea da Figueira é a mesma que fazia antigamente o serviço especial entre a fabrica da Marinha Grande e o porto de S. Martinho. 
O pinhal abunda no trajecto d'esta linha, logo que se passa a estação de Vallado: o famoso pinhal de Leiria, filho querido d'el-rei D. Diniz. 
Em toda esta região, que vamos atravessando, houve outr'ora porém um senhor ainda mais poderoso do que D. Diniz: era o mar. 
Alfeizerão, que deixamos ha muito, terra dentro, fôra até ao seculo XVI um bello porto de mar, que podia abrigar mais de oitenta embarcações. 
D. Diniz, que tinha em Monte Real a sua habitação predilecta, quiz fazer na villa de Paredes um porto de mar. Sobre as ruinas de Paredes assenta a actual Pederneira. 
Fez-se effectivamente o porto, porque, como diz o proverbio, _El-rei Diniz fez quanto quiz_, mas grandes alluviões de areia foram obstruindo o porto. D. Diniz pretendeu pôr um obstaculo a essas enormes alluviões mandando semear o pinhal de Leiria e adoptando outras providencias conducentes ao mesmo fim. 
Uma d'essas providencias consistiu em ordenar aos foreiros da casa da Nazareth que lançassem, contra o mar, um certo numero de carradas de areia, que o vento fosse accumulando no largo da egreja e nas ruas do Sitio. 
Da povoação de Paredes, que devia ser importante, graças ao movimento do seu porto, existe apenas... a Pederneira. 
O mar espraiou-se pois por toda esta região, que vamos atravessando, até que as alluviões de areia lhe disputaram o dominio. 
E hoje a locomotiva assobia o hymno do progresso atravez do pinhal de D. Diniz, deixando ao largo o mar, que perdemos de vista para só tornar a enxergal-o nas proximidades da Figueira da Foz. 
A estação que se segue á da Marinha Grande é a de Leiria. 
No alto do monte escarpado, o castello de Leiria, com a sua torre de menagem menos mal conservada, desenha-se no azul, immovel e sereno. 
Ha muitos annos que eu não tinha visto este vetusto castello, que me deixára uma impressão desagradavel quando então passei em Leiria caminho da Batalha. Nas ruas de Leiria o castello, esmagando-me com o peso dos seus muros e da sua torre, parecia seguir-me por toda a parte, como um fardo que me dobrava os hombros. Era asphyxiante, visto da cidade, aquelle castello. Mas, visto de longe, como agora, affigurou-se-me um dos mais bonitos castellos que sobrevivem ainda, gostei de vêl-o altivo na sua decadencia, magestoso ainda na sua inutilidade, esperando impassivel a hora em que a tempestade derrube os seus muros com um feixe de raios... 
Passado o apeadeiro dos Milagres, é Monte-Real, o sitio predilecto de D. Diniz, a primeira estação que se encontra. 
Apezar da ardencia d'esse dia, extremamente calmoso, apezar de ser oppressiva a temperatura, abafadiço o ar, passou pelo meu espirito um relampago de historia patria, vi de relance D. Diniz, trovador aventuroso, rei galante, envolvido nas suas proezas tunantescas de Leiria, frequentando de noite, enamorado de uma camponeza, a aldeia de Amor... 
El-rei Diniz Fez quanto quiz, Até no amor... Graças ao sceptro, Graças ao plectro, Rei-trovador. 
Depois, quiz-me parecer tolice de marca maior estar a remexer no rescaldo da historia amorosa de D. Diniz em dia de tão intensa calma, fechei de subito o livro da memoria, forcejei por lembrar-me de que eu tinha escolhido aquelle dia precisamente para não pensar em nada que me desse cuidado, e puz-me a olhar para a paizagem que ia apparecendo e fugindo como no fundo de um kaleidoscopo. 
A Amieira, que é o ponto de bifurcação do ramal de Alfarellos, possue, como se sabe, uma nascente de aguas medicinaes, que está sendo explorada com bons creditos. 
Ahi, encostadas á grade da estação, vimos as primeiras camponezas do valle do Mondego, com o seu trajo caracteristico,--bellos exemplares de opulencia plastica, e saudamos n'essas tres camponezas, sadias e robustas, a mulher do norte. 
E as tres camponezas, ouvindo ou não ouvindo as nossas saudações enthusiasticas, comiam maçãs, rilhando-as um pouco suinamente, ó prosa da realidade, terrivel prosa! 
Não se póde já ser um pouco artista, nem mesmo em viagem! 
O valle do Mondego principiou a desenrolar-se deante dos nossos olhos, com os seus esteiros, a sua linha recta coberta de verdura e scintillante de agua. 
Entre a Amieira e a Figueira medeiam apenas dois apeadeiros, o de Lares e o de Santo Aleixo: as primeiras casas da Figueira não tardaram a apparecer-nos como guarda avançada d'essa bonita cidade maritima, já então tão concorrida de banhistas. 
Na estação da Figueira entramos no _americano_ porque o meu amigo Carrilho propoz, e eu approvei, que fossemos antes de jantar a Buarcos. 
O _americano_ deslisa ao longo da praia. Deslisa é um modo de dizer, porque, justamente quando passavamos em frente do Bairro Novo, o _americano_ emperrou pela primeira vez, saltou fóra das calhas, um muar cahiu estatelado. Quando isto aconteceu pela segunda vez, no meio das pragas de varios hespanhoes que enchiam o carro, o meu amigo Carrilho propoz que fizéssemos a pé o passeio de Buarcos. 
E assim mesmo é que foi: largamos a andar por alli fóra intrepidamente. 
Buarcos é uma especie de retiro de banhistas pacatos, que fogem do bulicio da Figueira. Bom ar, bom mar, mas pouca gente. Pacato de mais. Quasi ao mesmo tempo que chegavamos a Buarcos, tendo feito o caminho a pé, chegava o _americano_, com os muares escalavrados das successivas quedas que tinham dado. 
O conductor perguntou-nos se queriamos ir vêr a mina do Cabo Mondego ou se faziamos tenção de ir vêr a fabrica. Dissemos-lhe que faziamos apenas tenção de ir jantar á Figueira. Então o conductor disse-nos que, visto termos ido a pé, tendo pago os nossos logares, queria de algum modo indemnisar-nos, fazendo-nos transportar immediatamente á Figueira. 
Pasmamos d'aquillo, d'aquelle original _americano_, tão caprichoso no seu serviço irregular! 
O carro partiu, e foi-se enchendo pelo caminho. Só então reconhecemos que tinhamos feito um grande passeio a pé, quasi sem dar por isso. 
Anoitecia. O céu e o mar estavam serenos. Um vaporzinho rebocava um navio, porque a barra da Figueira, apesar dos melhoramentos que se lhe têm feito, é simplesmente detestavel. No alto, o Bairro Novo alvejava com as suas construcções recentes, elegantes, e, ao trote dos muares, entramos de novo na Figueira, parando era frente do _Hotel Universal_. 
Esperámos, á janella do hotel, que nos servissem o jantar, e pudemos surprehender d'ahi a physionomia um pouco hybrida mas pittoresca da Figueira: o mar batia contra a muralha, o navio entrava rebocado, uns pescadores passavam altercando, e dois homens de chapeu alto e sobrecasaca passeiavam, conversando. Bastava effectivamente isto para caracterisar a Figueira com todo o seu ar pretencioso de cidade e o seu aspecto de praia de banhos, sendo que os da terra andam de chapéu alto, no grave exercicio das suas funcções judiciaes, administrativas, commerciaes, e os de fóra, os banhistas, em plena praia, exhibem fato de flanella branca e chapeu de côco. 
No _Hotel Universal_ jantaram apenas comnosco á mesa mais dois hospedes, ambos brazileiros, que estiraram desde a sopa até ao café uma conversa merencoria como elles, que ambos estavam doentes. 
Um dos dois, o mais sorumbatico de ambos, fallou da morte,--assumpto divertidissimo! Disse-nos que todas as noites, a bordo do paquete, quando se fazia silencio, a idéa da morte, passando pelo seu espirito, o atormentava. 
Eu perguntei ao meu amigo Carrilho o que tinhamos nós com aquillo? 
Concordámos em que não tinhamos nada, absolutamente nada, com os pavores phantasticos do brazileiro. Levantámo-nos da mesa; vimos um predio illuminado, ouvimos musica. 
Perguntamos ao criado que predio era aquelle. 
--É o theatro do Principe D. Carlos, e ha hoje espectaculo. 
Muito bem. Iriamos dar um passeio pela cidade, e cahiriamos depois no theatro. 
Todo o aspecto commercial da cidade estava então em evidencia: as lojas illuminadas, bellas lojas, devendo citar-se uma ourivesaria, que fazia lembrar um estabelecimento do Chiado. 
Na Praça Nova a colonia balnear, composta principalmente de hespanhoes, espanejava-se garrulamente, e em torno da praça as lojas de negocio, havendo ás portas grupos de homens, uns a pé, outros sentados, denunciavam o movimento commercial da cidade. 
Na Praça Nova encontrámos o deputado Pereira dos Santos e o visconde de Miranda do Corvo, que tiveram a amabilidade do nos ir mostrar os magnificos clubs da Figueira e de nos acompanhar ao theatro. 
No theatro havia pouca gente. Um prestidigitador, cujo nome me esqueceu, fazia umas _sortes_ sediças, com pouca limpeza. Mas o theatro fôra para nós um salvaterio, porque nos permittiu esperarmos ahi pela hora da partida do comboio, meia noite e vinte. 
E, n'um dos intervallos, entre muitos episodios da chronica balnear da Figueira, ouvi contar um, que me divertiu hilariantemente, e que no capitulo seguinte tentarei reproduzir. 
Á meia noite, quando sahimos do theatro, havia ainda luz nos clubs e nos cafés. As janellas das roletas e das batotas brilhavam com o clarão interior dos candieiros de petroleo, porque a cidade da Figueira só agora vae ser illuminada a gaz. 
Despedindo-nos dos nossos amigos visconde de Miranda do Corvo e Pereira dos Santos, dirigimo-nos para a estação do caminho de ferro, atravez de uma escuridade profunda, sem saber onde punhamos os pés, tropeçando a cada passo. 
Que reles economia a da Companhia, que fazendo um comboio depois da meia noite, não manda illuminar o caminho da estação! 
Ás tres horas e 46 minutos da manhã chegavamos ás Caldas da Rainha, frescos, apesar da caminhada a Buarcos, da estopada funebre do brazileiro, de duas horas de prestidigitação no theatro do Principe D. Carlos e dos trambolhões que démos em caminho da estação,--sem vermos um palmo adeante do nariz. 





