I Nas Caldas da Rainha 


Na quarta feira de manhã, o comboio em que eu vim para as Caldas da Rainha regorgitava de viajantes. 
E desde quarta feira não tenho visto senão chegar ás Caldas gente, gente, gente! 
Os hespanhoes vão mandando os seus primeiros contingentes, e d'aqui a poucos dias chegará o grosso do exercito. 
Principia a ouvir-se já esse infatigavel chalrar das hespanholas, que ao longo da alameda vão agitando a ventarola e... os quadris, passando como um bando de cigarras palreiras, seguidas de grande numero de cigarrinhas não menos garrulas do que as suas mamãs de olhos pretos e os seus papás de _patillas_. 
Muitas caras de Lisboa: pessoas da alta finança... com dyspepsia; e da grande roda... com rheumatismo. 
Algumas pessoas da provincia, com ar de principes que viajam sob incognito. 
Toda esta humanidade, mais ou menos espectaculosa, que passeia no Olympo da alameda, de tridente na mão, desce do alto da sua importancia ao razo da fragilidade do barro commum, logo que entra as portas da Copa. 
Ahi, perante as aguas, são todos eguaes. 
Portuguezas de chapeus de palha, hespanholas de mantilha, janotas do Chiado, anciãos venerandos, sentados em torno da casa das pulverisações, voltados contra a parede, de bocca escancarada, n'uma immobilidade paciente, deixam penetrar na garganta, em pequenissimas gottas de agua do tamanho de missangas, a aspersão d'esse hyssope therapeutico, que os ha de benzer... para o inverno. 
Vêl-os alli e imaginal-os devotos romeiros que estão collando os seus labios a uma nascente milagrosa de agua de Lourdes, é tudo uma e a mesma coisa. Reverentes deante da torneira, de _bibe_ de borracha em volta do pescoço e toalha de algodão sobre os joelhos, parece entoarem mentalmente um hymno védico em honra e louvor da agua das Caldas: «Gloria a ti nas torneiras, ó milagrosa agua, que borrifas a minha garganta, desces pelo meu esophago, penetras no meu corpo! Abençoada sejas tu e mais os sagrados Pintos Coelhos da medicina, que mandam que a gente te sorva em pequenas doses, tal qual como em Lisboa!» 
Mas, feita esta oração naturalistica, as damas, os janotas, os anciãos arremessam com desdem o seu _bibe_ de borracha, sacodem a toalha de algodão, e readquirem, á sahida da Copa, o seu bello ar mundano, parecendo dizer aos platanos da Alameda: «No reino animal, a que temos a honra de pertencer, não somos nada inferiores a vós outros, senhores ornamentos do reino vegetal!» 
E as andorinhas, que nas Caldas são em numero prodigioso, esvoaçando de platano para platano parece dizerem lá de cima: «Como v. ex.ª está fresco com as aguas! Viva v. ex.ª, e não falte cá para o anno!» 
Deante do Sebastião, que ministra os copinhos de agua das Caldas com a mesma gravidade com que Ganimedes devia servir Jupiter á mesa dos deuses, toda a gente tem um vago estremecimento do espirito, seja porque o Sebastião represente a saude ou a diplomacia, pois que realmente a saude é a diplomacia com que a gente quer tratar o corpo, e a diplomacia é a saude com que as nações pretendem curar as suas mazellas. 
É talvez por esta dupla representação que o Sebastião da Copa tem um certo ar solemne, ao mesmo tempo de medico e de diplomata, não sendo elle nada d'isso. 
Sebastião, antes de pegar no copo, do o lavar e de o encher, relanceia a sua pupilla verde, n'uma observação rapida mas profunda, pela pessoa que está deante de si. 
Estuda-a n'esse relance de olhos e, silenciosamente, como um soberano que dispensa mercês, dispensa elle copos d'agua, servindo-se do seu gesto grave como se fosse um decreto. 
Sim! a gente, tambem n'um relance, parece lêr isto nos seus olhinhos verdes: «Eu Sebastião, copeiro por graça de Deus, sou servido servir este copinho d'agua a este cavalheiro que o requer, com a circumstancia tacita de o julgar tolo, porque principia por tomar cincoenta grammas quando devia limitar-se a tomar apenas trinta. Mas eu, Sebastião, copeiro por graça de Deus, que estou sempre ao pé da agua, lavo d'ahi as minhas mãos,--silenciosamente.--Dada de bico callado aos tantos de tal, nas Caldas da Rainha e de copo na mão». 
A gente lê este decreto, toma o seu copinho, e sáe a porta. Respira-se então melhor,--como quando se sáe da Ajuda. Até ir repetir a dose ninguem pensa mais no Sebastião, porque é tambem esse um ruim sestro da natureza humana: depois de recebida a graça, ninguem pensa mais em quem lh'a concedeu. 
E os que receberam a agua no copo procedem similhantemente aos que receberam a agua em pulverisação, isto é, desoppremidos, _dessebastianados_, espanejam-se ao longo da Alameda rindo, conversando, como se gosassem a melhor das saudes e não tivessem tomado remedio algum ha muitos annos. 
A saude, que todos de manhã julgam perdida, reapparece á noite, florescente e agil, na valsa e mesmo no chá, entregando-se resolutamente aos compassos de Strauss e ás bolachas do Club. 
Ora este Club das Caldas,--pois que fallei n'elle,--parece-se um pouco com as praças de guerra: é dos primeiros que o occupam. Isto seria inteiramente justo, por direito de conquista, se os primeiros que chegam se limitassem a occupal-o,--mas fazem mais alguma coisa: entrincheiram-se em grupos. 
O grupo A arvóra bandeira, fortifica-se, e resiste. 
O grupo B hasteia egualmente a sua bandeira, fortifica-se, e... resiste. 
Os outros grupos fazem a mesma coisa. 
É um entrincheiramento geral. 
O melhor que ha a fazer, para tomar posse do Club das Caldas, é vir para cá no S. João. 
De resto é preciso escalar, fazer de Affonso Henriques deante de Santarem, trepar de gatas pela muralha, pendurar-se das ameias. A alguns pobres rapazes de dezoito annos, que ultimamente chegaram, temos visto realisar prodigios de acrobatismo caldense para treparem á muralha e arrancarem uma valsa. 
E, porque n'essa edade tudo esquece facilmente, depois de tão trabalhosa escalada sentem-se felizes girando em torno do salão, levando presa pela cintura uma dama que lhes custou tanto a conquistar como a formosa Rachel a seu primo Jacob. 
N'este caso, e sobretudo n'esta metaphora biblica, o tio Labão é representado pelo entrincheiramento dos grupos entre si. 
Ás vezes os Jacobs do Club não dançam precisamente com a prima Rachel, isto é, com aquella dama que elles prefeririam, porque essa tem-n'a o tio Labão fechada a sete chaves n'um grupo. Mas, para não perderem de todo o tempo, vão dançando com a Lia que por muito favor lhes concederam. 
A Matta é este anno um pouco abandonada. Não resiste á concorrencia que lhe fazem o Ceu do Vidro e a Alameda, onde agora mesmo, tres horas da tarde de domingo, ha numerosos grupos, conversando, jogando, observando. O amor, como um macaco na floresta, vae saltando de arvore em arvore, e, escondido entre as ramarias, despede settas certeiras, ficando a rir e a baloiçar-se nos ramos... 
Com a sua ligeireza simiana ora aponta a um seio turgido, afofado entre cambraias; ora, como que por ironia, dispara contra um peito já sabiamente abroquelado para estes combates. 
No primeiro caso, a setta crava-se no alvo, que fica ferido, gottejando sangue ardente. 
No segundo caso, resvala no broquel de aço e a dama, vendo cahir no chão a setta, fica dizendo mentalmente: «Para cá vens tu de carrinho!...» 
Em duas horas observa-se toda a vida das Caldas; por isso, inteirado da situação, tenho feito pequenas excursões fóra do mundo galante da Alameda. 
Fui á Lagôa de Obidos, que eu só conhecia nominalmente dos compendios de chorographia. 
Passeio delicioso, por uma bella estrada marginalmente povoada de pinheiros. 
Cheguei á Foz do Arelho ao cahir da tarde. A lagôa principiava a esbater-se na penumbra, n'uma doce tranquillidade. Os pescadores recolhiam nas bateiras, que singravam mansamente. Mulheres e creanças esperavam-n'os sentados na areia, mas as creanças, logo que viram aproximar-se um trem, fizeram-lhe um verdadeiro assalto, chegando a engalfinhar-se nas portinholas. 
E na grande paz da lagôa a primeira treva da noite ia cahindo como um véo de crepe, lentamente. 
Hontem fui de corrida á Nazareth. 
Ah! meu Deus! que desillusão! 
Caldas da Rainha, 5 de agosto de 1888. 





