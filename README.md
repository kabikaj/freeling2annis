
# POS tagging multilingual texts with Freeling and converting into Annis
Project for POS tagging texts using Freeling and converting them to an Annis database.

## Requirements
- python 3.5+
- python module bs4
- java 1.7+
- javac 1.7+
- postgresql
- freeling 4.0 (http://nlp.lsi.upc.edu/freeling/node/1)
- pepper (http://corpus-tools.org/pepper)
- annis (http://corpus-tools.org/annis)

## Installation
This installation has been tested on linux mint 17.3.  
Download Annis and pepper. Put pepper in the same folder of the project.
```bash
$ pip3 install --upgrade pip
$ sudo pip3 install setuptools
$ sudo pip3 install bs4

$ git clone https://github.com/TALP-UPC/FreeLing.git 
$ sudo apt-get install build-essential automake autoconf libtool g++
$ sudo apt-get install libboost-regex-dev libicu-dev zlib1g-dev
$ sudo apt-get install libboost-system-dev libboost-program-options-dev
$ sudo apt-get install libboost-thread-dev
$ autoreconf --install
$ ./configure && make && make install

$ sudo apt-get install postgresql
$ sudo -u postgres psql
postgres=# \password
postgres=# \q

$ sudo apt-get install default-jdk
```

## Example of usage
```bash
$ bash execute_workflow.sh -i data/homage_to_catalonia \
                           -x data/homage_to_catalonia_xml \
                           -a data/homage_to_catalonia_annis \
                           -l en \
                           -c homage_to_catalonia_corpus \
                           -m data/homage_to_catalonia.meta \
                           -v
```
where *data/homage_to_catalonia* contains several documents with the extension *.txt*, each having an optional document with exacly the same name but *.ini* extension containig the metadata of the corresponding txt document (these files will be parsed automatically in case they exist); *data/homage_to_catalonia_xml* will stored intermediate xml files; and *data/homage_to_catalonia_annis* will contain the annis database. The optional file "data/homage_to_catalonia.meta" contains metadata associated to the whole corpus.

After executing the workflow, import *data/homage_to_catalonia_annis* in to Annis and you will be able to see and query your corpus.

## Relevant links
- Help on freeling installation: https://github.com/mcolebrook/freeling
- List of languages supported by freeling: https://talp-upc.gitbooks.io/freeling-user-manual/content/basics.html
- Annis installation: http://korpling.github.io/ANNIS/doc/admin-install.html

## Contact
Alicia González Martínez , *aliciagm85+freeling2annis at gmail dot com*

